import { RequestHandler, Request, Response, NextFunction } from 'express';
import OrderService, { OrderListPayload } from '../services/order.service';
import { PeriodType } from '../shared/enums/periodType';
import { AlltimeRevenue, ForecastNextYearRevenue, LastMonthRevenue, LastYearRevenue, GenericRevenue } from '../shared/models/revenueTypes';
import { PagedOrderList } from '../shared/models/pagedOrderList';
import { OrdersCount } from '../shared/models/orderTypes';

const orderService = new OrderService();

export const getRevenueLastYear: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const lastYearRevenue: LastYearRevenue = {
      lastYearRevenue: await orderService.getRevenueSumOfLastYear()
    }; 

    res.json(lastYearRevenue);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export async function getRevenueOfLastMonth(req: Request, res: Response) {
  try {
    const lastMonthRevenue: LastMonthRevenue = {
      lastMonthRevenue: await orderService.getRevenueSumOfLastMonth()
    };

    res.json(lastMonthRevenue);
  } catch(error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getRevenueAlltime: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const data: AlltimeRevenue = {
      alltimeRevenue: await orderService.getRevenueSumOfAlltime()
    };

    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getRevenueForecastOfNextYear: RequestHandler = async (req: Request, res: Response) => {
  try {
    const data: ForecastNextYearRevenue = {
      forecast: await orderService.getRevenueForecastNextYear()
    };

    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getRevenueChartData: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { periodTypeParam } = req.params;
    let periodType: PeriodType = PeriodType[periodTypeParam.toUpperCase() as keyof typeof PeriodType];

    if (periodType === undefined)
      return res.status(400).json({ error: 'invalid_period' });

    const chartData = await orderService.getRevenueChartDataOfPeriod(periodType);
    const dateMinMax = orderService.getMinAndMaxDate(chartData);

    res.json({ dateMinMax, chartData });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getProfitChartData: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { periodTypeParam } = req.params;
    let periodType: PeriodType = PeriodType[periodTypeParam.toUpperCase() as keyof typeof PeriodType];

    if (periodType === undefined)
      return res.status(400).json({ error: 'invalid_period' });

    const chartData = await orderService.getProfitChartDataOfPeriod(periodType);
    const dateMinMax = orderService.getMinAndMaxDate(chartData);

    res.json({ dateMinMax, chartData });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getPagedOrdersList: RequestHandler = async (req: Request, res: Response) => {
  try {
    let { page, take } = req.query;

    if (page === undefined)
      page = '1';

    if (take === undefined)
      take = '30';

    if (Number.isNaN(Number(page)))
      return res.status(400).json({ error: 'page_param_is_nan' });

    if (Number.isNaN(Number(take)))
      return res.status(400).json({ error: 'take_param_is_nan' });

    if (Number(page) < 1)
      return res.status(400).json({ error: 'page_param_outofbounds' });

    if (Number(take) < 1)
      return res.status(400).json({ error: 'take_param_outofbounds' });

    const list: OrderListPayload = await orderService.list(Number(take), Number(page));

    const data: PagedOrderList = {
      pageNumber: Number(page),
      resultLength: list.data.length,
      totalLength: list.total,
      orderList: list.data
    };

    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getCountOfOrdersCurrentDay: RequestHandler = async (req: Request, res: Response) => {
  try {
    const currentDayOrdersCount: OrdersCount = {
      ordersCount : await orderService.getCountOfOrdersCurrentDay()
    }; 

    res.json(currentDayOrdersCount);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getProfitOnDate: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { date } = req.params;
    const dateObject: Date = new Date(date);

    if (Object.prototype.toString.call(dateObject) === "[object Date]") {
      if (isNaN(dateObject.valueOf())) {
        return res.status(400).json({ error: 'date_object_not_valid' });
      } else {
        const profitOnDate: GenericRevenue = {
          revenue : await orderService.getProfitOnDate(dateObject)
        }; 

        res.json(profitOnDate);
      }
    } else {
      return res.status(400).json({ error: 'not_a_date_object' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getRevenueOnDate: RequestHandler = async (req: Request, res: Response) => {
  try {
    const { date } = req.params;
    const dateObject: Date = new Date(date);

    if (Object.prototype.toString.call(dateObject) === '[object Date]') {
      if (isNaN(dateObject.getTime())) { 
        return res.status(400).json({ error: 'date_not_valid' });
      } else {
        const revenueOnDate: GenericRevenue = {
          revenue : await orderService.getRevenueOnDate(dateObject)
        }; 
        
        res.json(revenueOnDate);
      }
    } else {
      return res.status(400).json({ error: 'not_date_object' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}
