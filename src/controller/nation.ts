import { RequestHandler, Request, Response, NextFunction } from 'express';
import NationService from '../services/nation.service';
import { NationTopOrderModel } from '../shared/models/nationTopOrderModel';

const nationService = new NationService();

export const getTopMostOrdersByNation: RequestHandler = async (req: Request, res: Response) => {
    try {
      const nationTopOrderModel: NationTopOrderModel = await nationService.getTopMostOrdersByNation();

      res.json(nationTopOrderModel);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'server_error' });
    }
  }
