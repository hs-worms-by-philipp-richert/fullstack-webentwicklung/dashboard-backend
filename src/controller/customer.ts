import { RequestHandler, Request, Response, NextFunction } from "express";
import CustomerService from '../services/customer.service';
import { CustomerRevenueModel } from "../shared/models/customerRevenueModel";
import { CustomerOrderCountModel } from "../shared/models/customerOrderCountModel";
import { PagedCustomerList } from "../shared/models/pagedCustomerList";

const customerService = new CustomerService();

export const getTopRevenue: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { limit } = req.params;

    if (isNaN(Number(limit)))
      return res.status(400).json({ error: 'limit_nan' });

    if (Number(limit) > 50)
      return res.status(400).json({ error: 'limit_too_high' });

    if (Number(limit) <= 0)
      return res.status(400).json({ error: 'limit_must_be_positive' });

    const list: CustomerRevenueModel[] = await customerService.getTopRevenueCustomerList(Number(limit));

    res.json(list);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error' });
  }
}

export const getTopOrders: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { limit } = req.params;

    if (isNaN(Number(limit)))
      return res.status(400).json({ error: 'limit_nan' });

    if (Number(limit) > 50)
      return res.status(400).json({ error: 'limit_too_high' });

    if (Number(limit) <= 0)
      return res.status(400).json({ error: 'limit_must_be_positive' });

    const list: CustomerOrderCountModel[] = await customerService.getTopOrdersList(Number(limit));

    res.json(list);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'server_error '});
  }
}


export const getPagedCustomerList: RequestHandler = async (req: Request, res : Response) => {
  try {
    let { page, take } = req.query;
    
    if (page === undefined)
      page = '1';

    if (take === undefined)
      page = '30';

    if (Number.isNaN(Number(page)))
      return res.status(400).json({ error : 'page_param_is_nan'});

    if (Number.isNaN(Number(take)))
      return res.status(400).json({ error : 'take_param_is_nan'});

    if (Number(page) < 1)
      return res.status(400).json({ error : 'page_param_outofbounds'});

    if (Number(take) < 1)
      return res.status(400).json({ error : 'take_param_outofbounds'});
    
    const list = await customerService.list(Number(take), Number(page));

    const data: PagedCustomerList = {
      pageNumber: Number(page),
      totalLength: list.total,
      customerList: list.data,
    };

    res.json(data);

  } catch (error) {
    console.error(error);
    res.status(500).json({error: 'server_error'})
  }
}
