import { CRUD } from '../interfaces/crud';
import { AppDataSource } from '../data-source';
import { Nation } from '../entity/Nation';
import { Order } from '../entity/Order';
import { Customer } from '../entity/Customer';
import { NationTopOrderModel } from '../shared/models/nationTopOrderModel';

class NationService implements CRUD {
  async list(limit: number, page: number) {
    throw new Error('not implemented');
  }
  
  async create(resource: any) {
    throw new Error('not implemented');
  }

  async putById(id: string, resource: any): Promise<any> {
    throw new Error('not implemented');
  }

  async readById(id: string) {
    throw new Error('not implemented');
  }

  async deleteById(id: string): Promise<any> {
    throw new Error('not implemented');
  }

  async patchById(id: string, resource: any): Promise<any> {
    throw new Error('not implemented');
  }

  async getTopMostOrdersByNation(): Promise<NationTopOrderModel> {
    const { nation_name, rowcount } = await AppDataSource.getRepository(Order)
            .createQueryBuilder('order')
            .leftJoinAndSelect('order.custkey', 'customer')
            .leftJoinAndSelect('customer.nationkey', 'nation')
            .select('nation.name')
            .addSelect('COUNT(*)', 'rowcount')
            .groupBy('nation.name')
            .orderBy('rowcount', 'DESC')
            .getRawOne()

    const data: NationTopOrderModel = {
        nationName: nation_name,
        orderCount: rowcount
    };

    return data;

  }
}

export default NationService;
