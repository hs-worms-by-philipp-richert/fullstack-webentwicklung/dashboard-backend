import { CRUD } from '../interfaces/crud';
import { AppDataSource } from '../data-source';
import { Order } from '../entity/Order';
import mathHelper from '../helper/mathHelper';
import dateHelper from '../helper/dateHelper';
import { PartSupp } from '../entity/PartSupp';
import { PeriodType } from '../shared/enums/periodType';
import { LineItem } from '../entity/LineItem';
import { SelectQueryBuilder } from 'typeorm';
import { OrderPartSuppModel } from '../shared/models/orderPartSuppModel';
import { ChartTuple } from '../shared/models/chartTuple';
import chartDataHelper from '../helper/chartDataHelper';
import { MinMaxTuple } from '../shared/models/minMaxTuple';
import { GenericRevenue } from '../shared/models/revenueTypes';
import { join } from 'path';
import { profile } from 'console';

export type OrderListPayload = {
  data: Order[];
  total: number;
};

class OrderService implements CRUD {
  async list(limit: number, page: number): Promise<OrderListPayload> {
    const skip = (page - 1) * limit;
    const [data, total] = await AppDataSource.getRepository(Order).findAndCount({
      take: limit,
      skip,
      relations: ['custkey', 'custkey.nationkey', 'lineitems'],
      order: { orderdate: 'DESC' }
    });

    return { data, total };
  }

  async create(resource: any) {
    throw new Error('not implemented');
  }

  async putById(id: string, resource: any): Promise<any> {
    throw new Error('not implemented');
  }

  async readById(id: string) {
    throw new Error('not implemented');
  }

  async deleteById(id: string): Promise<any> {
    throw new Error('not implemented');
  }

  async patchById(id: string, resource: any): Promise<any> {
    throw new Error('not implemented');
  }

  async getRevenueSumOfLastYear(): Promise<number> {
    let currentYear = (new Date()).getFullYear();
    let lastYear = currentYear - 1;

    let { sum } = await AppDataSource.getRepository(Order)
      .createQueryBuilder('order')
      .select('SUM(order.totalprice)', 'sum')
      .where('EXTRACT(YEAR FROM order.orderdate) = :lastYear', { lastYear })
      .getRawOne();

    return sum ?? 0;
  }

  async getRevenueSumOfLastMonth(shiftMonthsBackBy?: number): Promise<number> {
    const currentDate: Date = new Date();
    let year = currentDate.getFullYear();
    let month = currentDate.getMonth();  // months in JavaScript start at 0 (for January)

    // if shiftMonthsBackBy is set
    if (shiftMonthsBackBy !== undefined) {
      month -= shiftMonthsBackBy;
    }

    if (month <= 0) {
      year--;
      month = 12 + month; // if month is negative (due to shiftMonthsBackBy param), it will subtract by that amount
    }

    const { lastMonthRevenue } = await AppDataSource
      .getRepository(Order)
      .createQueryBuilder('order')
      .where('EXTRACT(MONTH FROM order.orderdate) = :month', { month })
      .andWhere('EXTRACT(YEAR FROM order.orderdate) = :year', { year })
      .select('SUM(order.totalprice)', 'lastMonthRevenue')
      .getRawOne();

    return lastMonthRevenue ?? 0;
  }

  async getRevenueSumOfAlltime(): Promise<number> {
    let { sum } = await AppDataSource.getRepository(Order)
      .createQueryBuilder('order')
      .select('SUM(order.totalprice)', 'sum')
      .getRawOne();

    return sum ?? 0;
  }

  async getRevenueForecastNextYear(): Promise<number> {
    const lastYearRevenue: number = await this.getRevenueSumOfLastYear();

    const revenueA: number = await this.getRevenueSumOfLastMonth();
    const revenueB: number = await this.getRevenueSumOfLastMonth(1);

    if (revenueA === 0)
      return 0;

    const growthRate = (revenueB - revenueA) / revenueA;

    const forecastRevenue = lastYearRevenue * (1 + growthRate);

    return mathHelper.round(forecastRevenue, 2);
  }

  async getRevenueChartDataOfPeriod(periodType: PeriodType): Promise<ChartTuple[]> {
    let periodWhere: string = chartDataHelper.getPeriodWhere(periodType);

    const orderListSelect: string = '"order".orderkey, "order".totalprice, "order".orderdate';

    const orderList: SelectQueryBuilder<Order> = await AppDataSource
      .getRepository(Order)
      .createQueryBuilder('order')
      .where(periodWhere);
    
    const rawOrderList: Order[] = await orderList.select(orderListSelect).getRawMany();

    const chartDataRaw: ChartTuple[] = chartDataHelper.convertOrderToChartTuple(rawOrderList);

    const chartDataSummed: ChartTuple[] = chartDataHelper.sumDuplicateDateValues(chartDataRaw);

    return chartDataSummed
  }

  async getProfitChartDataOfPeriod(periodType: PeriodType): Promise<ChartTuple[]> {
    let periodWhere: string = chartDataHelper.getPeriodWhere(periodType);

    const orderListSelect: string = '"order".orderkey, "order".totalprice, "order".orderdate';
    const lineItemListSelect: string = '"line_item".partkey, "line_item".suppkey, "line_item".orderkey, "line_item".quantity';
    const partSuppListSelect: string = '"part_supp".partkey, "part_supp".suppkey, "part_supp".supplycost';

    const orderList: SelectQueryBuilder<Order> = await AppDataSource
      .getRepository(Order)
      .createQueryBuilder('order')
      .where(periodWhere);
    
    const lineItemList: SelectQueryBuilder<LineItem> = await AppDataSource
      .getRepository(LineItem)
      .createQueryBuilder('line_item')
      .where(`"line_item".orderkey IN (${orderList.select('"order".orderkey').getQuery()})`);

    const partSuppList: SelectQueryBuilder<PartSupp> = await AppDataSource
      .getRepository(PartSupp)
      .createQueryBuilder('part_supp')
      .where(`"part_supp".partkey IN (${lineItemList.select('"line_item".partkey').getQuery()})`)
      .andWhere(`"part_supp".suppkey IN (${lineItemList.select('"line_item".suppkey').getQuery()})`);

    const rawOrderList: Order[] = await orderList.select(orderListSelect).getRawMany();
    const rawLineItemList: LineItem[] = await lineItemList.select(lineItemListSelect).getRawMany();
    const rawPartSuppList: PartSupp[] = await partSuppList.select(partSuppListSelect).getRawMany();

    const joinedData: OrderPartSuppModel[] = chartDataHelper.joinData(rawOrderList, rawLineItemList, rawPartSuppList);

    const chartDataRaw: ChartTuple[] = chartDataHelper.calculateProfit(joinedData);

    const chartDataSummed: ChartTuple[] = chartDataHelper.sumDuplicateDateValues(chartDataRaw);

    return chartDataSummed ?? [];
  }

  getMinAndMaxDate(chartData: ChartTuple[]): MinMaxTuple {
    const min: number = chartData.reduce((prev, curr) => prev.date < curr.date ? prev : curr)?.date ?? Date.now();
    const max: number = chartData.reduce((prev, curr) => prev.date > curr.date ? prev : curr)?.date ?? Date.now();

    return { min, max };
  }

  async getCountOfOrdersCurrentDay(): Promise<number> {
    const currentDate: Date = new Date();
    let year = currentDate.getFullYear();
    let month = currentDate.getMonth() + 1;
    let day = currentDate.getDate();

    let { ordercount } = await AppDataSource
      .getRepository(Order)
      .createQueryBuilder('order')
      .select('COUNT(*)', 'ordercount')
      .where('EXTRACT(DAY FROM order.orderdate) = :day', { day })
      .andWhere('EXTRACT(MONTH FROM order.orderdate) = :month', { month })
      .andWhere('EXTRACT(YEAR FROM order.orderdate) = :year', { year })
      .getRawOne();

    return ordercount ?? 0;
  }

  async getProfitOnDate(date: Date): Promise<number> {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    const orderData = await AppDataSource
      .getRepository(LineItem)
      .createQueryBuilder('lineitem')
      .leftJoinAndSelect('lineitem.orderkey', 'order')
      .leftJoinAndSelect('lineitem.partkey', 'partsupp')
      .select('order.totalprice', 'revenue')
      .addSelect('lineitem.quantity', 'qty')
      .addSelect('partsupp.supplycost', 'suppcost')
      .where(`EXTRACT(YEAR FROM orderdate) = ${year} AND EXTRACT(MONTH FROM orderdate) = ${month} AND EXTRACT(DAY FROM orderdate) = ${day}`)
      .getRawMany();

      let profit = 0;

      for (let i = 0; i < orderData.length; i++) {
        const revenue = orderData[i].revenue;
        const quantity = orderData[i].qty;
        const supplycost = orderData[i].suppcost;
  
        const singleprofit = revenue - (quantity * supplycost);
        
        profit += singleprofit;
      }

    return profit;
  }

  async getRevenueOnDate(date: Date): Promise<number> {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();
    
    let { revenue } = await AppDataSource
      .getRepository(Order)
      .createQueryBuilder('order')
      .where('EXTRACT(DAY FROM order.orderdate) = :day', { day })
      .andWhere('EXTRACT(MONTH FROM order.orderdate) = :month', { month })
      .andWhere('EXTRACT(YEAR FROM order.orderdate) = :year', { year })
      .select('SUM(order.totalprice)', 'revenue')
      .getRawOne();

    return revenue ?? 0;
  }
}

export default OrderService;
