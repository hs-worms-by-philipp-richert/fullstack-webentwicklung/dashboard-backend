import { CRUD } from '../interfaces/crud';
import { AppDataSource } from '../data-source';
import { Customer } from '../entity/Customer';
import { Order } from '../entity/Order';
import { CustomerRevenueModel } from '../shared/models/customerRevenueModel';
import { CustomerOrderCountModel } from '../shared/models/customerOrderCountModel';

class CustomerService implements CRUD {
  async list(limit: number, page: number) {
    const skip = (page - 1) * limit;
    const [data, total] = await AppDataSource.getRepository(Customer)
      .findAndCount({
        skip: skip,
        take: limit,
        relations: {
          nationkey: true,
        },
        order: { name : 'ASC'},
      })

    return { data, total };
  }
  
  async create(resource: any) {
    throw new Error('not implemented');
  }

  async putById(id: string, resource: any): Promise<any> {
    throw new Error('not implemented');
  }

  async readById(id: string) {
    throw new Error('not implemented');
  }

  async deleteById(id: string): Promise<any> {
    throw new Error('not implemented');
  }

  async patchById(id: string, resource: any): Promise<any> {
    throw new Error('not implemented');
  }
  
  async getTopRevenueCustomerList(limit: number): Promise<CustomerRevenueModel[]> {
    const topOrderList = await AppDataSource.getRepository(Order)
            .createQueryBuilder('order')
            .select('order.custkey')
            .addSelect('SUM(order.totalprice)', 'userrevenue')
            .groupBy('order.custkey')
            .orderBy('userrevenue', 'DESC')
            .limit(limit)
            .getRawMany();
    
    const customerList: Customer[] = [];

    for (let i = 0; i < topOrderList.length; i++) {
      const custkey = topOrderList[i].order_custkey;

      const customer = await AppDataSource.getRepository(Customer).findOne({
        select: {
          custkey: true,
          name: true
        },
        where: { custkey }
      });

      customerList.push(customer!); // customer kann aufgrund der foreign-key Verbindung nicht NULL sein
    }

    const list: CustomerRevenueModel[] = topOrderList.map(obj => {
      return {
        revenue: obj.userrevenue ?? 0,
        ...customerList.find(m => m.custkey == obj.order_custkey)!
      };
    });
    
    return list;
  }

  async getTopOrdersList(limit: number): Promise<CustomerOrderCountModel[]> {
    const topOrderByCountList = await AppDataSource.getRepository(Order)
            .createQueryBuilder('order')
            .select('order.custkey')
            .addSelect('COUNT(*)', 'rowcount')
            .groupBy('order.custkey')
            .orderBy('rowcount', 'DESC')
            .limit(limit)
            .getRawMany();

    const customerOrderList: Customer[] = [];

    for (let i = 0; i < topOrderByCountList.length; i++) {
      const custkey = topOrderByCountList[i].order_custkey;

      const customer = await AppDataSource.getRepository(Customer).findOne({
        select: {
          custkey: true,
          name: true
        },
        where: { custkey }
      });

      customerOrderList.push(customer!);
    }

    const list: CustomerOrderCountModel[] = topOrderByCountList.map(obj => {
      return {
        ...customerOrderList.find(m => m.custkey == obj.order_custkey)!,
        orderCount: obj.rowcount
      };
    });

    return list;

  }

}

export default CustomerService;
