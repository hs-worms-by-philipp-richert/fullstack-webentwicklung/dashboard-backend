import express, { Express, Request, Response, Application } from 'express';
import dotenv from 'dotenv';
import { AppDataSource } from './data-source';
import orderRoutes from './routes/orders';
import customerRoutes from './routes/customer';
import nationRoutes from './routes/nation';

import cors from 'cors';

dotenv.config();

const app: Application = express();
const port = process.env.PORT || 3000;

// Establish database connection
AppDataSource.initialize()
  .then(() => {
    console.log('Data Source has been initialized!');
  })
  .catch((error) => console.error('Error during Data Source initialization:', error));

// express configuration and routes
app.use(express.json());
app.use(cors());

app.use('/orders', orderRoutes);
app.use('/customer', customerRoutes);
app.use('/nation', nationRoutes);

app.get('/', (req: Request, res: Response) => {
  res.send('Management Cockpit API - Project by Bartosz Durczak, Kevin Hanst, Maik Krawczyk, and Philipp Richert');
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
