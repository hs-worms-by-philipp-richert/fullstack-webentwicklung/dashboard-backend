import { LineItem } from "../entity/LineItem";
import { Order } from "../entity/Order";
import { PartSupp } from "../entity/PartSupp";
import { PeriodType } from "../shared/enums/periodType";
import { ChartTuple } from "../shared/models/chartTuple";
import { OrderPartSuppModel } from "../shared/models/orderPartSuppModel";
import dateHelper from "./dateHelper";
import mathHelper from "./mathHelper";

const getPeriodWhere = (periodType: PeriodType): string => {
  let periodWhere: string;
    
  const currentDate: Date = new Date();
  const monthParam: number = currentDate.getMonth() + 1;
  const yearParam: number = currentDate.getFullYear();

  switch (periodType) {
    case PeriodType.MONTH:
      periodWhere = `EXTRACT(YEAR FROM orderdate) = ${yearParam} AND EXTRACT(MONTH FROM orderdate) = ${monthParam}`;
      break;
    case PeriodType.YEAR:
      periodWhere = `EXTRACT(YEAR FROM orderdate) = ${yearParam}`;
      break;
    case PeriodType.ALLTIME:
      periodWhere = 'TRUE';
      break;
    default:
      throw new Error('invalid_periodtype');
  }

  return periodWhere;
}

const joinData = (rawOrderList: Order[], rawLineItemList: LineItem[], rawPartSuppList: PartSupp[]): OrderPartSuppModel[] => {
  const joinedData: OrderPartSuppModel[] = rawLineItemList.map<OrderPartSuppModel>((lineItemObj): OrderPartSuppModel => {
    const joinedObj: OrderPartSuppModel = {
      ...lineItemObj,
      ...rawPartSuppList.find(partSuppObj => partSuppObj.partkey === lineItemObj.partkey && partSuppObj.suppkey === lineItemObj.suppkey) ?? { supplycost: 0 },
      ...rawOrderList.find(orderObj => orderObj.orderkey === lineItemObj.orderkey) ?? { totalprice: 0 }
    };

    return joinedObj;
  });

  return joinedData;
}

const calculateProfit = (joinedData: OrderPartSuppModel[]): ChartTuple[] => {
  const chartDataRaw: ChartTuple[] = joinedData.filter(r => r.orderdate).map<ChartTuple>((obj): ChartTuple => {
    const profit = obj.totalprice - obj.supplycost*obj.quantity;

    return {
      value: mathHelper.round(profit, 2),
      date: dateHelper.trimTime(obj.orderdate)!.getTime()
    };
  });

  return chartDataRaw;
}

const convertOrderToChartTuple = (rawData: Order[]): ChartTuple[] => {
  return rawData.filter(r => r.orderdate).map<ChartTuple>((obj): ChartTuple => {
    return {
      value: mathHelper.round(obj.totalprice, 2),
      date: dateHelper.trimTime(obj.orderdate)!.getTime()
    };
  });
}

const sumDuplicateDateValues = (chartDataRaw: ChartTuple[]): ChartTuple[] => {
  chartDataRaw.sort((a, b) => a.date < b.date ? -1 : 1);

  const chartDataSummed = [chartDataRaw[0]];
  for (const item of chartDataRaw) {
    const sumItem = chartDataSummed.at(-1);

    if ((sumItem?.date ?? 0) === item.date && sumItem) {
      sumItem.value += item.value;
    } else {
      chartDataSummed.push(item);
    }
  }

  return chartDataSummed;
}

export default {
  getPeriodWhere,
  joinData,
  calculateProfit,
  convertOrderToChartTuple,
  sumDuplicateDateValues
};
