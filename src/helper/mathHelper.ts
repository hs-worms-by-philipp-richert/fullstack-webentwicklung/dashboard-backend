class MathHelper {
  round(num: number, decimalPlaces: number = 0): number {
    let p: number = Math.pow(10, decimalPlaces);
    let n: number = (num * p) * (1 + Number.EPSILON);

    return Math.round(n) / p;
  }
}

export default new MathHelper();
