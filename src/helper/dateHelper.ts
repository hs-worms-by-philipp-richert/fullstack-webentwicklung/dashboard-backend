class DateHelper {
  trimTime(datetime?: Date): Date | undefined {
    if (!datetime)
      return;

    const newDate: Date = datetime;
    newDate.setHours(0, 0, 0, 0);
    return newDate;
  }
}

export default new DateHelper();
