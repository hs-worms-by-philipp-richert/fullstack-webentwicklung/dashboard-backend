import { Router } from 'express';
import { getTopRevenue, getTopOrders, getPagedCustomerList } from '../controller/customer';

const router = Router();

router.get('/top-revenue/:limit', getTopRevenue);

router.get('/top-orders/:limit', getTopOrders);

router.get('/list', getPagedCustomerList);

export default router;
