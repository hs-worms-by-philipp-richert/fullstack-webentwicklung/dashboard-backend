import { Router } from 'express';
import { getTopMostOrdersByNation } from '../controller/nation';

const router = Router();

router.get('/top-most-orders', getTopMostOrdersByNation);

export default router;