import { Router } from 'express';
import {
  getRevenueLastYear,
  getRevenueOfLastMonth,
  getRevenueAlltime,
  getRevenueForecastOfNextYear,
  getProfitChartData,
  getPagedOrdersList,
  getCountOfOrdersCurrentDay,
  getRevenueChartData,
  getProfitOnDate,
  getRevenueOnDate
} from '../controller/orders';

const router = Router();

router.get('/list', getPagedOrdersList);

router.get('/revenue/lastmonth', getRevenueOfLastMonth);

router.get('/revenue/alltime', getRevenueAlltime);

router.get('/revenue/lastyear', getRevenueLastYear);

router.get('/revenue/forecast/nextyear', getRevenueForecastOfNextYear);

router.get('/revenue/chart-data/:periodTypeParam', getRevenueChartData);

router.get('/revenue/on/:date', getRevenueOnDate);

router.get('/profit/chart-data/:periodTypeParam', getProfitChartData);

router.get('/profit/on/:date', getProfitOnDate);

router.get('/count/today', getCountOfOrdersCurrentDay);

export default router;
