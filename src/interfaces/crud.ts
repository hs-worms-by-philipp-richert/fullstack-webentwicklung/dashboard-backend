export interface CRUD {
  list: (limit: number, page: number) => Promise<any>;

  create: (resource: any) => Promise<any>;

  putById: (id: any, resource: any) => Promise<boolean>;

  readById: (id: any) => Promise<any>;

  deleteById: (id: any) => Promise<boolean>;

  patchById: (id: any, resource: any) => Promise<boolean>;
}
