import { Column, Entity, PrimaryColumn, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';
import { Order } from './Order';
import { Part } from './Part';
import { PartSupp } from './PartSupp';
import { ILineItem } from '../shared/interfaces/ILineItem';

@Entity()
export class LineItem implements ILineItem {
  @ManyToOne(() => Order, order => order.orderkey)
  @JoinColumn({ name: "orderkey" })
  @PrimaryColumn("bigint")
  orderkey: bigint

  @ManyToOne(() => PartSupp, partSupp => partSupp.partkey)
  @ManyToOne(() => PartSupp, partSupp => partSupp.suppkey)
  @JoinColumn([
    { name: "partkey", referencedColumnName: "partkey" },
    { name: "suppkey", referencedColumnName: "suppkey" }
  ])
  @Column("bigint")
  partkey: bigint

  @Column("bigint")
  suppkey: bigint

  @PrimaryGeneratedColumn({ type: "bigint" })
  linenumber: bigint

  @Column("double precision")
  quantity: number

  @Column("numeric")
  extendedprice: number

  @Column("numeric")
  discount: number

  @Column("numeric")
  tax: number

  @Column("char", { length: 1 })
  returnflag: string

  @Column("char", { length: 1 })
  linestatus: string

  @Column("date")
  shipdate: Date

  @Column("date")
  commitdate: Date

  @Column("date")
  receiptdate: Date

  @Column("char", { length: 25 })
  shipinstruct: string

  @Column("char", { length: 10 })
  shipmode: string

  @Column("varchar", { length: 44, nullable: true })
  comment: string
}
