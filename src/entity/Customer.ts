import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Nation } from './Nation';
import { ICustomer } from '../shared/interfaces/ICustomer';

@Entity()
export class Customer implements ICustomer {
  @PrimaryGeneratedColumn("uuid")
  custkey: string

  @Column("varchar", { length: 25 })
  name: string

  @Column("varchar", { length: 40 })
  address: string

  @ManyToOne(() => Nation, nation => nation.nationkey)
  @JoinColumn({ name: 'nationkey' })
  @Column("int")
  nationkey: Nation

  @Column("char", { length: 15 })
  phone: string

  @Column("numeric")
  acctbal: number

  @Column("char", { length: 10 })
  mktsegment: string

  @Column("varchar", { length: 117, nullable: true })
  comment: string
}
