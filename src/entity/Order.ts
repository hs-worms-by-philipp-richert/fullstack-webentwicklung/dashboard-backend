import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Customer } from './Customer';
import { IOrder } from '../shared/interfaces/IOrder';
import { LineItem } from './LineItem';

@Entity()
export class Order implements IOrder {
  @PrimaryGeneratedColumn({ type: "bigint" })
  orderkey: bigint

  @ManyToOne(() => Customer, customer => customer.custkey)
  @JoinColumn({ name: "custkey" })
  @Column("uuid")
  custkey: string

  @Column("char", { length: 1 })
  orderstatus: string

  @Column("numeric")
  totalprice: number

  @Column("date")
  orderdate: Date

  @Column("char", { length: 15 })
  orderpriority: string

  @Column("char", { length: 15 })
  clerk: string

  @Column("int")
  shippriority: number

  @Column("varchar", { length: 79, nullable: true })
  comment: string

  @OneToMany(() => LineItem, (lineitem) => lineitem.orderkey)
  lineitems: LineItem[]
}
