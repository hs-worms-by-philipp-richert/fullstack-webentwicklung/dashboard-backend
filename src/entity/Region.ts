import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IRegion } from '../shared/interfaces/IRegion';

@Entity()
export class Region implements IRegion {
  @PrimaryGeneratedColumn({ type: "int" })
  regionkey: number

  @Column("char", { length: 25 })
  name: string

  @Column("varchar", { length: 152, nullable: true })
  comment: string
}
