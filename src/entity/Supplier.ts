import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Nation } from './Nation';
import { ISupplier } from '../shared/interfaces/ISupplier';

@Entity()
export class Supplier implements ISupplier {
  @PrimaryGeneratedColumn({ type: "bigint" })
  suppkey: bigint

  @Column("char", { length: 25 })
  name: string

  @Column("varchar", { length: 40 })
  address: string

  @ManyToOne(() => Nation, nation => nation.nationkey)
  @JoinColumn({ name: "nationkey" })
  @Column("int")
  nationkey: number

  @Column("char", { length: 15 })
  phone: string

  @Column("numeric")
  acctbal: number

  @Column("varchar", { length: 101, nullable: true })
  comment: string
}
