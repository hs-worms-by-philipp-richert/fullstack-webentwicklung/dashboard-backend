# Dashboard Backend


## Description
This project has been developed as part of a graded fullstack-webdevelopment semester-project at the University of Applied Sciences in Worms. The goal of this project was to implement a series of [requirements](requirements.md) *(R01-R06)*, where we also added a few extras *(R07-R14)*.

The goal of this project was to create a management cockpit that makes use of the [TPC-H Benchmark](https://www.tpc.org/tpch/). As a [team of four](#authors) *(Bartosz Durczak, Kevin Hanst, Maik Krawczyk, and Philipp Richert)*, we worked on this project over the span of approximately two months.

Some technical prerequisities were, that we had to use Node.js along with an ORM for the backend, Angular for the frontend, and a containerized database of any kind. We further expanded on this by using Node.js with TypeScript, TypeORM for our ORM, and a dockerized PostgreSQL container. 

A detailed walk-through of our project-structure can be found [below](#project-structure).

## Table of Contents
[[_TOC_]]

## Project Structure
The project is divided into two main repositories: **dashboard-backend** and **dashboard-frontend**.

We also have a **dashboard-shared** that is a submodule inside backend and frontend, and also a **dashboard-devtools** to generate test-data.

The `docker-compose.yml` is stored inside the backend-repository.

A Postman collection with all the API endpoints can also be found inside `postman/Management_Dashboard.postman_collection.json`.

## Installation
> :paperclip: **Note:** Throughout this chapter, when referencing a file path, repositories will be marked with square backets around them to differentiate them from folder names as part of a path.

### Before getting started
Should have installed:
- Docker and docker-compose
- Node.js and npm

### Cloning the repositories
We will need to clone three repositories. Enter a working directory of your choice and execute the following clone commands:

1. Backend Repository
    ```
    git clone git@gitlab.ai.it.hs-worms.de:fullstack-webapplications-wise-2023/group-x/dashboard-backend.git
    ```
2. Frontend Repository
    ```
    git clone git@gitlab.ai.it.hs-worms.de:fullstack-webapplications-wise-2023/group-x/dashboard-frontend.git
    ```
3. Devtools Repository
    ```
    git clone git@gitlab.ai.it.hs-worms.de:fullstack-webapplications-wise-2023/group-x/dashboard-devtools.git
    ```

We have now successfully cloned the main repositories. However, we are now still missing the shared-submodule inside backend and frontend repositories.

> :warning: **The following steps must be done twice:** inside `dashboard-backend` and `dashboard-frontend` folder!

1. 
    ```
    git submodule init
    ```
2. 
    ```
    git submodule update
    ```

The code is now completely cloned and almost ready to run.

### Database
A docker-compose file can be found at `[dashboard-backend]/database/docker-compose.yml`. To make the use of docker-compose more efficient in the terminal, this project has two npm-scripts inside `[dashboard-backend/package.json]` for this:
```
npm run db-start
```
and
```
npm run db-stop
```

1. Run `npm run db-start` to initialize and start the database container.
2. Run `docker ps` to make sure postgres and Adminer (simple SQL admin panel) are running.
3. Check `http://localhost:8080` to see if Adminer can access our database without issues.

### Backend
With our database set up, we can now run our backend. But first, make sure the credentials in `src/data-source.ts` are the same as in `database/docker-compose.yml`.

Now run the command:
```
npm i
```
followed by:
```
npm run dev
```

The program will connect to our database, and TypeORM will create the tables inside of it. The last output in the terminal should be:
> "Data Source has been initialized!".

In the next step, we will insert some test data.

### Inserting Test-Data
First, copy the contents of the file `[dashboard-backend]/database/INSERT_region_nation.sql` and execute this SQL code in your database.
Next, go into the `dashboard-devtools` directory and again check if the credentials inside `src/data-source.ts` match those of our database.

Then run the command:
```
npm i
```
followed by:
```
npx ts-node src/index.ts --table=<name>
```
The above command must be executed for all table names each in the following order: `customer`, `orders`, `supplier`, `part`, `partsupp`, and `lineitem`.

The expected output is:
> "Success! Data has been saved to the database."

> :paperclip: **Note:** one may ask why the devtools-project does not make use of our shared directory. This is because this script was originally only supposed to be temporary, but then decided it is too useful to discard. We also wanted it to be completely separate from the main projects, as to not affect them in any way. 

### Frontend
With our backend and database out of the way, the only thing left is our frontend. Go into the `dashboard-frontend` directory and, again, type the command:

```
npm i
```
this time followed by:
```
ng serve
```

Everything should now have started seamlessly, and the website should now be accessible at `http://localhost:4200`.

### Deployment
To build the backend for production, run the command:
```
npm run build
```
to transpile our TypeScript code into JavaScript code. The resulting build can then be ran in any preferred way: container, pm2, or others.

To build the frontend for production, run the command:
```
ng build
```

> :warning: Make sure to clean up any test-data and set a more secure database password before going into production!

## Running Tests
If you want to make sure everything is working as expected, we have prepared a few unit tests for our backend and frontend projects.

Inside `dashboard-backend`, run:
```
npm run test
```

Inside `dashboard-frontend`, run:
```
ng test
```

> :warning: The frontend-test requires Chrome to be installed!

## Authors
- Bartosz Durczak
    - Email: [bartoszdurczak2001@gmail.com](mailto:bartoszdurczak2001@gmail.com)
- Kevin Hanst
    - Email: [kevinhanst2706@gmail.com](mailto:kevinhanst2706@gmail.com)
- Maik Krawczyk
    - Email: [maik.krw@gmail.com](mailto:maik.krw@gmail.com)
- Philipp Richert
    - [LinkedIn](https://de.linkedin.com/in/philipp-richert)
    - Email: [contact@philipp-richert.me](mailto:contact@philipp-richert.me)
    - Websites: [byflip.dev](https://byflip.dev), [philipp-richert.me](https://philipp-richert.me)

---
