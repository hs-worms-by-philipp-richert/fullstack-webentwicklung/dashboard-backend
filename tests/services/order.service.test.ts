import {describe, expect, it, jest} from '@jest/globals';
import OrderService from '../../src/services/order.service';

jest.mock('../../src/services/order.service');

const MockOrderService = jest.mocked(OrderService);

describe('order service mocked unit-test', () => {
  const orderService = new OrderService();

  it('returns value for revenue of all-time', async () => {
    MockOrderService.prototype.getRevenueSumOfAlltime.mockImplementationOnce(() => {
      return new Promise(resolve => resolve(5000));
    });

    const result = await orderService.getRevenueSumOfAlltime();

    expect(result).toBe(5000);
  });

  it('returns value for revenue of last year', async () => {
    MockOrderService.prototype.getRevenueSumOfLastYear.mockImplementationOnce(() => {
      return new Promise(resolve => resolve(10000));
    });

    const result = await orderService.getRevenueSumOfLastYear();

    expect(result).toBe(10000);
  });
});
