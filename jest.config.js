module.exports = {
  preset: 'ts-jest',
  testMatch: [
    '**/*.test.ts'
  ],
  testEnvironment: 'node',
  setupFiles: ['dotenv/config']
};
