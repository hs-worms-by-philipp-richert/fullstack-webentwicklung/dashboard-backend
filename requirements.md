# List of Requirements
- [x] **R01:** "which is the total overall revenue?"
- [x] **R02:** "what is the revenue of the last month?"
- [x] **R03:** "what is the revenue of the last year?"
- [x] **R04:** "what revenue is expected for the next year and why?"
- [x] **R05:** "who are the top 3 best customers with the most revenue?"
- [x] **R06:** "who are the top 5 customers with the most orders?"

## Additional Requirements/Features
- [x] **R07:** get daily chart data for month, year, and all-time
- [x] **R08:** get count of orders for the current day
- [x] **R09:** get nation name with the most orders
- [x] **R10:** get (paged!) list of all orders
- [x] **R11:** get (paged!) list of all customers
- [x] **R12:** get revenue for specific day (calendar date-select)
- [x] **R13:** get profit for specific day (calendar date-select)
- [x] **R14:** get chart for revenue
