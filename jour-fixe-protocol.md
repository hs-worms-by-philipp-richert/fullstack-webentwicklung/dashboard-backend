# Jour Fixes - Protocol

## 2023-10-16 - Kickoff
- [x] Initialization of backend project
- [x] Creation of basic HTTP server in Node.js TypeScript

## 2023-10-21 - Jour Fixe
- [x] Create Docker container for Postgres database
- [x] Setup of database table structure
- [x] Go over the project structure and explain how API endpoints are implemented
- [x] Talk about ORMs (will probably use TypeORM?)
- Appended: short Q&A about Node, the development process, and what's to come for this project

## 2023-10-28 - Jour Fixe
- [x] Address new project structure due to TypeORM entities
- [x] Quick explanation/walkthrough of TypeORM usage
- [x] Creation of basic Angular project (using cli tool)
- [ ] Walkthrough of Angular

## 2023-11-01 - Mid-week meeting for task allocation
- [ ] Walkthrough of Angular
- [x] [Task](requirements.md) allocation:
  - Durczak: R02
  - Hanst: R01
  - Krawczyk: R03
  - Richert: R05 & prepare Angular base

## 2023-11-04 - Jour Fixe
- [x] Discussion of the tasks
- [x] Merge dev branches
- [ ] Talk about frontend design *(postponed)*
- [x] Introduction of services into the project structure 
- [x] [Task](requirements.md) allocation:
  - Hanst & Krawczyk: R06
  - Richert: R04 (consultation with Kohler required) & Angular base

## 2023-11-08 - Mid-week meeting
- [x] [Task](requirements.md) allocation:
  - Durczak: frontend for R02

## 2023-11-11 - Jour Fixe
- [x] Discussion of the tasks
- [x] Merge dev branches
- [x] Talk about frontend design
- [x] Sketch/Discuss design layout
- [x] [Task](requirements.md) allocation:
  - Durczak: frontend component for R01, R02, and R03
  - Hanst: frontend component for R04
  - Krawczyk: frontend component for R05 and R06
  - Richert: frontend component for R07 and [create devtools](https://gitlab.ai.it.hs-worms.de/fullstack-webapplications-wise-2023/group-x/dashboard-devtools)

## 2023-11-18 - Jour Fixe
- [x] Discussion of the tasks
- [x] Merge dev branches
- [x] Discuss further implementations
  - ~~World map for origin of orders?~~
  - List of all orders (paged)?
- [ ] [Task](requirements.md) allocation:
  - Durczak: list all customers (page)
  - Hanst: new orders today (dashboard)
  - Krawczyk: country with most orders (dashboard)
  - Richert: list all orders (page)

## 2023-11-25 - Jour Fixe
- [x] Discussion of the tasks
- [x] Merge dev branches
- [x] [Task](requirements.md) allocation:
  - Durczak: add burger menu button
  - Hanst: revenue for specific day (dashboard)
  - Krawczyk: profit for specific day (dashboard)
  - Richert: revenue graph (dashboard) & case study: can echarts be made zoomable?

## 2023-12-01 - Jour Fixe
- [x] Discussion of the tasks
- [x] Merge dev branches
- [x] Talk about test cases

## 2023-12-04 - Jour Fixe
- [ ] Task allocation:
  - Durczak: (frontend) revenue-overview.component.ts
  - Hanst: (frontend) orders-today.component.ts
  - Krawczyk: (backend) orderService.getRevenueSumOfLastYear()
  - Richert: (backend) orderService.getRevenueSumOfAlltime()

## 2023-12-09 - Jour Fixe
- [ ] Write README files
- [ ] Final code review

## 2023-12-16 - Finished?
- /
